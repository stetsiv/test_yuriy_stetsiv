import org.junit.Test;import org.openqa.selenium.WebDriver;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import pageobjects.LoanPage;
import pageobjects.MainPage;
import pageobjects.MortgagePage;
import tools.BrowserDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Yura on 17.06.2017.
 */
public class BaseTest {

    private WebDriver driver ;

    @BeforeClass
    public void setUp(){
        driver = BrowserDriver.getInstance();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://ia.ca/");
        new MainPage().goToPage2();
        new LoanPage().goToPage3();
        new MortgagePage().goToPage4();
    }


    @AfterClass
    public void tearDown(){
        BrowserDriver.stop();
    }
}
