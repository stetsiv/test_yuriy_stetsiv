package webelements.calculationpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Yura on 19.06.2017.
 */
public class AmortisationDropDown {
    private WebElement element1;
    private WebElement element2;

    public AmortisationDropDown(WebElement element1, WebElement element2) {
        this.element1 = element1;
        this.element2 = element2;
    }

    public void selectAmortisation(String years) {
        this.element1.click();
        List<WebElement> options = element2.findElements(By.tagName("li"));
        for (WebElement option : options) {
            if (option.getText().contains(years)) {
                option.click();
            }
        }
    }

}
