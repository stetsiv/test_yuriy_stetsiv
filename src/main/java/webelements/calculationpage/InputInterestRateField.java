package webelements.calculationpage;

import org.openqa.selenium.WebElement;

/**
 * Created by Yura on 19.06.2017.
 */
public class InputInterestRateField {
    private WebElement element;

    public InputInterestRateField(WebElement element) {

        this.element = element;
    }

    public void typeRate(double rate) {

        this.element.click();
        this.element.clear();
        this.element.sendKeys((String.valueOf(rate)));
    }
}
