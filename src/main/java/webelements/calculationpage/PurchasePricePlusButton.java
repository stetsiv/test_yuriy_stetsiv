package webelements.calculationpage;

import org.openqa.selenium.WebElement;

/**
 * Created by Yura on 18.06.2017.
 */
public class PurchasePricePlusButton {
    private WebElement element;
    private int price;
    public final static int MAX_SLIDER_PRICE = 2000000;
    public final static int SLIDER_STEP = 250000;


    public PurchasePricePlusButton(WebElement element, int price) {
        this.element = element;
        this.price = price;
    }

    public void click() {
        for (int i = 0; i < MAX_SLIDER_PRICE; i += SLIDER_STEP) {
            if (i != price) {
                this.element.click();
            } else {
                break;
            }
        }
    }
}
