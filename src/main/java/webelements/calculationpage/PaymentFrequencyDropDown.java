package webelements.calculationpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Yura on 19.06.2017.
 */
public class PaymentFrequencyDropDown {
    private WebElement element1;
    private WebElement element2;

    public PaymentFrequencyDropDown(WebElement element1, WebElement element2) {
        this.element1 = element1;
        this.element2 = element2;
    }

    public void selectPaymentFrequency(String frequency) {
        this.element1.click();
        List<WebElement> options = element2.findElements(By.tagName("li"));
        for (WebElement option : options) {
            if (option.getText().equals(frequency)) {
                option.click();
            }
        }
    }
}
