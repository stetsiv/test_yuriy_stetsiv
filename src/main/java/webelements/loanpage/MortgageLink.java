package webelements.loanpage;

import org.openqa.selenium.WebElement;

/**
 * Created by Yura on 19.06.2017.
 */
public class MortgageLink {
    private WebElement element;

    public MortgageLink(WebElement element) {
        this.element = element;
    }

    public void click(){
        this.element.click();
    }
}
