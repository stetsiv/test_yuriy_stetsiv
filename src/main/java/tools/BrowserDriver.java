package tools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Created by Yura on 17.06.2017.
 */
public class BrowserDriver {

    private static ChromeDriver driver;

    private BrowserDriver() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            synchronized (BrowserDriver.class) {
                if (driver == null) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("--lang=en-us");
                    driver = new ChromeDriver(options);
                }
            }
        }
        return driver;
    }

    public static void stop(){
        if(driver!=null){
            driver.quit();
            driver = null;
        }
    }
}

